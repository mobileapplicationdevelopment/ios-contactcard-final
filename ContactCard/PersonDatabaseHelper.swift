//
//  PersonDatabaseHelper.swift
//  ContactCard
//
//  Created by Luuk Ros on 03/11/2016.
//  Copyright © 2016 Luuk Ros. All rights reserved.
//

import Foundation

class PersonDatabaseHelper : NSObject {
    
    static let sharedInstance = PersonDatabaseHelper()
    
    var dataBase : OpaquePointer? = nil
    
    // Establishing connection with database
    override init() {
        super.init()
        
        let bundlePath = Bundle.main.url(forResource: "person", withExtension: "sqlite")
        let docPath = getDocumentsDirectory().appendingPathComponent("person.sqlite")
        
        if !FileManager.default.fileExists(atPath: docPath.path) {
            try! FileManager.default.copyItem(at: bundlePath!, to: docPath)
        }
        
        if sqlite3_open(docPath.path, &dataBase) != SQLITE_OK {
            print("Error while opening the PersonsDB")
        }
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    // INSERT INTO
    func insertPerson(gender: String, title: String, firstName: String, lastName: String, email: String, phoneNumber: String, cellNumber: String, street: String, city: String, state: String, thumbnail: String) {
        
//        let query = "INSERT INTO person (gender, title, firstName, lastName, email, phoneNumber, cellNumber, street, city, state, thumbnail) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
        
        let query = "INSERT INTO person (gender) VALUES (?)"
        
        var statement : OpaquePointer? = nil
        
        var status = sqlite3_prepare(dataBase, query, -1, &statement, nil)
        let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
        
        if status == SQLITE_OK {
            sqlite3_bind_text(statement, 1, gender, -1, SQLITE_TRANSIENT);
//            sqlite3_bind_text(statement, 2, title, -1, nil)
//            sqlite3_bind_text(statement, 3, firstName, -1, nil)
//            sqlite3_bind_text(statement, 4, lastName, -1, nil)
//            sqlite3_bind_text(statement, 5, email, -1, nil)
//            sqlite3_bind_text(statement, 6, phoneNumber, -1, nil)
//            sqlite3_bind_text(statement, 7, cellNumber, -1, nil)
//            sqlite3_bind_text(statement, 8, street, -1, nil)
//            sqlite3_bind_text(statement, 9, city, -1, nil)
//            sqlite3_bind_text(statement, 10, state, -1, nil)
//            sqlite3_bind_text(statement, 11, thumbnail, -1, nil)
    
            status = sqlite3_step(statement)
            
            if status != SQLITE_DONE {
                let errorMessage = String(cString: sqlite3_errmsg(dataBase))
                print(errorMessage)
                print("Error inserting row")
            }
        } else {
            print("Error")
        }
        
        sqlite3_reset(statement)
        sqlite3_finalize(statement)
        
    }
    
    // READ FROM
    func readPerson() -> [Person]? {
        
        var persons = [Person]()
        
        let query = "SELECT * FROM person;"
        
        var statement : OpaquePointer? = nil
        
        if sqlite3_prepare_v2(dataBase, query, -1, &statement, nil) != SQLITE_OK {
            
            let errorMessage = String.init(describing: sqlite3_errmsg(dataBase))
            print("Error while executing query: \(errorMessage)")
            
        } else {
            
            while sqlite3_step(statement) == SQLITE_ROW {
                
                let person = Person()
                
                person.gender = String(cString: sqlite3_column_text(statement, 1))
                person.title = String(cString: sqlite3_column_text(statement, 2))
                person.firstName = String(cString: sqlite3_column_text(statement, 3))
                person.lastName = String(cString: sqlite3_column_text(statement, 4))
                person.email = String(cString: sqlite3_column_text(statement, 5))
                person.phoneNumber = String(cString: sqlite3_column_text(statement, 6))
                person.cellNumber = String(cString: sqlite3_column_text(statement, 7))
                person.street = String(cString: sqlite3_column_text(statement, 8))
                person.city = String(cString: sqlite3_column_text(statement, 9))
                person.state = String(cString: sqlite3_column_text(statement, 10))
                person.thumbnail = String(cString: sqlite3_column_text(statement, 11))
                
                persons.append(person)
            
            }
        
        }
        
        return persons
        
    }
    
}

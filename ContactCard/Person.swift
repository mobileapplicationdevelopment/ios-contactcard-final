//
//  Person.swift
//  ContactCard
//
//  Created by Luuk Ros on 08/09/16.
//  Copyright © 2016 Luuk Ros. All rights reserved.
//

import Foundation
import UIKit

class Person : NSObject {
    // Gender
    var gender : String?
    
    // Full Name
    var title : String?
    var firstName : String?
    var lastName : String?
    
    // Contact
    var email : String?
    var phoneNumber : String?
    var cellNumber : String?
    
    // Location
    var street : String?
    var city : String?
    var state : String?
//    var postcode : Int?
    
    // Image
    var thumbnail : String?
    
}

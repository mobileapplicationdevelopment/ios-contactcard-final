//
//  PersonTableViewController.swift
//  ContactCard
//
//  Created by Luuk Ros on 08/09/16.
//  Copyright © 2016 Luuk Ros. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class PersonTableViewController: UITableViewController {
    
    let url : String = "https://randomuser.me/api/"
    let apiKey = ""
    
    var persons : [Person] = []
    
    // Setting up Outlet & Action for addPersonButton
    @IBOutlet weak var addPersonButton: UIButton!
    @IBAction func addPersonButton(_ sender: UIButton) {
        self.getPerson()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for _ in 0..<1 {
            self.getPerson()
        }
        
        let personDatabaseHelper = PersonDatabaseHelper.sharedInstance
        self.persons = personDatabaseHelper.readPerson()!
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.persons.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "personCell", for: indexPath) as! PersonTableViewCell
        
        let person = persons[indexPath.row]
        
        cell.personTitle.text = person.title
        cell.personFirstName.text = person.firstName
        cell.personLastName.text = person.lastName
        
        Alamofire.request(person.thumbnail!,
                          method: .get).responseData { (response : DataResponse<Data>) in
                            switch(response.result) {
                                
                            case .success(_):
                                let personThumbnail = UIImage(data: response.result.value!)
                                cell.personThumbnail.image = personThumbnail
                                break;
                                
                            case .failure(_):
                                break;
                                
                            }
                            
        }
        
        return cell
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "personDetail" {
            if let destination = segue.destination as? PersonDetailViewController {
                if let indexPath = self.tableView.indexPathForSelectedRow {
                    let person = persons[(indexPath as NSIndexPath).row]
                    destination.person = person
                }
            }
        }
    }
    
    func getPerson() {
        
        // (Optional) URL Params
        let parameters = [String : String]()
        
        // (Optional) Headers
        let headers = [String : String]()
        
        // Performing the .GET-method
        Alamofire.request(url,
                          method: .get,
                          parameters: parameters,
                          encoding: URLEncoding.default,
                          headers: headers).responseJSON { response in
                            
                            // JSON-parsing
                            if let json = response.result.value as? Dictionary<String, Any> {
                                
                                // Creating a person-object for storing person-data
                                let person = Person()
                                
                                // Getting all the results from the request
                                let allResults = json["results"] as! NSArray
                                
                                // Splitting individual results from all the results of the request
                                let singleResult = allResults[0] as! NSDictionary
                                
                                // Getting & parsing the gender-object from a single result
                                let gender = singleResult.object(forKey: "gender") as! String
                                
                                person.gender = gender
                                
                                // Getting & parsing the nested objects inside name-object from a single result
                                let fullName = singleResult.object(forKey: "name") as! NSDictionary
                                let title = fullName.object(forKey: "title") as! String
                                let firstName = fullName.object(forKey: "first") as! String
                                let lastName = fullName.object(forKey: "last") as! String
                                
                                person.title = title
                                person.firstName = firstName
                                person.lastName = lastName
                                
                                // Getting & parsing the nested objects inside location-object from a single result
                                let location = singleResult.object(forKey: "location") as! NSDictionary
                                let street = location.object(forKey: "street") as! String
                                let city = location.object(forKey: "city") as! String
                                let state = location.object(forKey: "state") as! String
                                // let postcode = location.object(forKey: "postcode") as! Int
                                
                                person.street = street
                                person.city = city
                                person.state = state
                                // person.postcode = postcode
                                
                                // Getting & parsing various contact-objects from a single result
                                let email = singleResult.object(forKey: "email") as! String
                                let phoneNumber = singleResult.object(forKey: "phone") as! String
                                let cellNumber = singleResult.object(forKey: "cell") as! String
                                
                                person.email = email
                                person.phoneNumber = phoneNumber
                                person.cellNumber = cellNumber
                                
                                // Getting & parsing the nested objects inside picture-object from a single result
                                let pictures = singleResult.object(forKey: "picture") as! NSDictionary
                                let thumbnail = pictures.object(forKey: "large") as! String
                                
                                person.thumbnail = thumbnail
                                
                                self.persons.append(person)
                                
                                self.tableView.reloadData()
                                
                                let personDatabaseHelper = PersonDatabaseHelper.sharedInstance
                                personDatabaseHelper.insertPerson(gender: person.gender!, title: person.title!, firstName: person.firstName!, lastName: person.lastName!, email: person.email!, phoneNumber: person.phoneNumber!, cellNumber: person.cellNumber!, street: person.street!, city: person.city!, state: person.state!, thumbnail: person.thumbnail!)
                                
                            }
                            
                            print(self.persons)
                            
        }
    }
    
}
        

//
//  PersonTableCellView.swift
//  ContactCard
//
//  Created by Luuk Ros on 08/09/16.
//  Copyright © 2016 Luuk Ros. All rights reserved.
//

import UIKit

class PersonTableViewCell : UITableViewCell {
    
    @IBOutlet weak var personThumbnail: UIImageView!
    @IBOutlet weak var personTitle: UILabel!
    @IBOutlet weak var personFirstName: UILabel!
    @IBOutlet weak var personLastName: UILabel!
    
    var person : Person?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        personThumbnail.layer.cornerRadius = 64
        personThumbnail.clipsToBounds = true
        personThumbnail.layer.borderColor = UIColor.white.cgColor
        personThumbnail.layer.borderWidth = 5
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  PersonDetailViewController.swift
//  ContactCard
//
//  Created by Luuk Ros on 08/09/16.
//  Copyright © 2016 Luuk Ros. All rights reserved.
//

import UIKit
import Alamofire

class PersonDetailViewController: UIViewController {
    
    @IBOutlet weak var personName: UILabel!
    @IBOutlet weak var personThumbnail: UIImageView!
    @IBOutlet weak var personGender: UILabel!
    @IBOutlet weak var personEmail: UILabel!
    @IBOutlet weak var personPhone: UILabel!
    @IBOutlet weak var personCell: UILabel!
    @IBOutlet weak var personStreet: UILabel!
    @IBOutlet weak var personCity: UILabel!
    @IBOutlet weak var personState: UILabel!
    @IBOutlet weak var personPostcode: UILabel!
    @IBOutlet var infoButton: UIButton!
    @IBOutlet weak var infoView: UIView!
    
    var person : Person?
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidLoad() {
        let thumbnail = person?.thumbnail
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        infoView.isHidden = true
        
        personName.text = (person?.title)! + " " + (person?.firstName)! + " " + (person?.lastName)! as String?
        
        personThumbnail.image = nil
        
        Alamofire.request(thumbnail!,
                          method: .get).responseData { (response : DataResponse<Data>) in
                            switch(response.result) {
                                
                            case .success(_):
                                let thumbnail = UIImage(data: response.result.value!)
                                self.personThumbnail.image = thumbnail
                                break;
                                
                            case .failure(_):
                                break;
                            }
        }
        
        personThumbnail.layer.cornerRadius = 65
        personThumbnail.layer.borderColor = UIColor.white.cgColor
        personThumbnail.layer.borderWidth = 5.0
        personThumbnail.clipsToBounds = true
        
        personGender.text = person?.gender
        personStreet.text = person?.street
        personCity.text = person?.city
        personState.text = person?.state
        personEmail.text = person?.email
        personPhone.text = person?.phoneNumber
        personCell.text = person?.cellNumber
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func infoButton(_ sender: UIButton) {
        infoView.isHidden = !infoView.isHidden
        
        infoButton.setTitle(infoView.isHidden ? "Show more" : "Show less", for: UIControlState())
    }

}
